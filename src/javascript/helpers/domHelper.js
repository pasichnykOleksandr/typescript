export function createElement(el) {
    const element = document.createElement(el.tagName);
    if (el.className) {
        const classNames = el.className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }
    if (el.attributes) {
        Object.keys(el.attributes).forEach((key) => element.setAttribute(key, el.attributes[key]));
    }
    return element;
}
