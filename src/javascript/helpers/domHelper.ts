export interface Element {
  tagName: string;
  className?: string;
  attributes: { [index: string]: string; };
  style?: any;
}

export function createElement(el: Element) {
  const element = document.createElement(el.tagName);

  if (el.className) {
    const classNames = el.className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  if(el.attributes){
    Object.keys(el.attributes).forEach((key) => element.setAttribute(key, el.attributes[key]));
  }

  return element;
}
