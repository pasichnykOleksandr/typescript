import { controls } from '../../constants/controls';
import { DetailFighter } from "../helpers/mockData"

export async function fight(firstFighter: DetailFighter, secondFighter: DetailFighter, firstEvent: KeyboardEvent) {
  const [firstMaxHealth, secondMaxHealth] = [firstFighter.health, secondFighter.health];
  return new Promise((resolve) => {
    let pressedKeys: string[] = [];
    getPressedKey(firstEvent);

    function getPressedKey(event: KeyboardEvent){
      pressedKeys.push(event.code);
    }

    function sendPressedKeys(){
      if(pressedKeys.length !== 0){
        pressedKeys = [...new Set(pressedKeys)];
        playRound(pressedKeys, firstFighter, secondFighter, firstMaxHealth, secondMaxHealth);
        if(isFinished(firstFighter, secondFighter)){
          window.removeEventListener('keydown', getPressedKey, false);
          window.removeEventListener('keyup', sendPressedKeys, false);
          firstFighter.health <= 0  ? resolve(secondFighter) : resolve(firstFighter);
        }
        pressedKeys = [];
      }
    }

    window.addEventListener('keydown', getPressedKey, false);
    window.addEventListener('keyup', sendPressedKeys, false);
  });
}

function isFinished(firstFighter: DetailFighter, secondFighter: DetailFighter){
  return firstFighter.health <= 0 || secondFighter.health <= 0;
}

export function getDamage(attacker: DetailFighter, defender: DetailFighter) {
  let attackValue = getHitPower(attacker);
  let defenseValue = getBlockPower(defender)
  return  defenseValue >= attackValue ? 0 : attackValue - defenseValue;
}

export function getHitPower(fighter: DetailFighter) {
  let randomNumber = Math.random() + 1;
  return fighter.attack * randomNumber;
}

export function getComboPower(fighter: DetailFighter) {
  return fighter.attack * 2;
}

export function getBlockPower(fighter: DetailFighter) {
  let randomNumber = Math.random() + 1;
    return fighter.defense * randomNumber;
}

export function isCombo(combination: string, pressedKeys: string[], previousTime: Date, currentTime: Date, playerIndex: number){
  let isComboAttack = controls[combination].every((v: any) => pressedKeys.includes(v));
  if(!isComboAttack){return [false, pressedKeys]};
  controls[combination].forEach((key: any) => {
    pressedKeys.splice(pressedKeys.indexOf(key), 1);
  });

  if(isComboAttack && isValidInterval(previousTime, currentTime)){
    pressedKeys.push(playerIndex === 1 ? 'PlayerOneCombo' : 'PlayerTwoCombo');
    previousTime = currentTime;
  }

  return [isComboAttack, pressedKeys, previousTime];
};

export function isValidInterval(previousTime: Date, currentTime: Date){
  if(previousTime === undefined) return true;
  let seconds = (currentTime.getMilliseconds() - previousTime.getMilliseconds()) / 1000;
  return seconds >= 10 ? true : false;
}

let previousTimeFirst: Date, previousTimeSecond: Date;

export function playRound(pressedKeys: string[], firstFighter: DetailFighter, secondFighter: DetailFighter, firstMaxHealth: number, secondMaxHealth: number) {
  let isComboFirst, isComboSecond, firstFighterAction, secondFighterAction;
  let currentTime = new Date();

  [isComboFirst, pressedKeys, previousTimeFirst] = [...isCombo('PlayerOneCriticalHitCombination', pressedKeys, previousTimeFirst, currentTime, 1)];
  [isComboSecond, pressedKeys, previousTimeSecond] = [...isCombo('PlayerTwoCriticalHitCombination', pressedKeys, previousTimeSecond, currentTime, 2)];
  
  firstFighterAction = chooseOneAction(pressedKeys, 1);
  secondFighterAction = chooseOneAction(pressedKeys, 2);
  calculateRound(firstFighterAction, secondFighterAction, firstFighter, secondFighter, firstMaxHealth, secondMaxHealth);
}

export function chooseOneAction(pressedKeys: string[], fighterIndex: number){
  let fighterAction: string;

  if(pressedKeys.includes(fighterIndex === 1 ? 'PlayerOneCombo' : 'PlayerTwoCombo')){
    fighterAction = 'combo';
    return fighterAction;
  }
  
  if(pressedKeys.includes(fighterIndex === 1 ? controls.PlayerOneBlock : controls.PlayerTwoBlock)){
    fighterAction = 'block';
    return fighterAction;
  }

  if(pressedKeys.includes(fighterIndex === 1 ? controls.PlayerOneAttack : controls.PlayerTwoAttack)){
    fighterAction = 'attack';
    return fighterAction;
  }

  return '';
}

export function calculateRound(firstAction: string, secondAction: string, firstFighter: DetailFighter, secondFighter: DetailFighter, firstMaxHealth: number, secondMaxHealth: number){
  let damage;
  
  if(firstAction === 'block' && secondAction !== 'combo') return 0;
  if(secondAction === 'block' && firstAction !== 'combo') return 0;

  if(firstAction === 'combo' || firstAction === 'attack'){
    firstAction === 'combo' ? damage = getComboPower(firstFighter) : damage = getDamage(firstFighter, secondFighter);
    secondFighter.health -= damage;
    renderBar('#right-fighter-indicator', secondFighter.health, secondMaxHealth);
  }

  if(secondAction === 'combo' || secondAction === 'attack'){
    secondAction === 'combo' ? damage = getComboPower(secondFighter) : damage = getDamage(secondFighter, firstFighter);
    firstFighter.health -= damage;
    renderBar('#left-fighter-indicator', firstFighter.health, firstMaxHealth);
  }
}

export function renderBar(barId: string, health: number, maxhealth: number){
  let bar: any = document.querySelector(barId);
  if(health <= 0){
    bar.style.width = 0;
  }else{
    bar.style.width = ((100*health)/maxhealth).toFixed(0) + '%';
  }
}