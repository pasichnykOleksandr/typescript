import { Element, createElement } from '../../helpers/domHelper';

export interface Modal {
  title: string;
  bodyElement?: HTMLDivElement,
  onClose?: () => void,
}

export function showModal({ title, bodyElement, onClose = () => document.location.href="/" }: Modal,) {
  
  let root: any = getModalContainer();
  const modal = createModal({ title, bodyElement, onClose}); 
  
  root.append(modal);
}

function getModalContainer() {
  return document.getElementById('root');
}

function createModal({ title, bodyElement, onClose }: Modal) {
  const layer = createElement({ tagName: 'div', className: 'modal-layer', attributes: {} });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root', attributes: {} });

  const header = createHeader(title, onClose);

  modalContainer.append(header, bodyElement || '');
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose: any) {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header', attributes: {} });
  const titleElement = createElement({ tagName: 'span', attributes: {} });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn', attributes: {} });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  
  const close = () => {
    hideModal();
    onClose();
  }
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);
  
  return headerElement;
}

function hideModal() {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
